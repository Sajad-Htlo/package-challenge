package com.mobiquityinc;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.packer.Packer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;

/**
 * Unit tests for the {@linkplain Packer}
 *
 * @author sajad
 */
@RunWith(JUnit4.class)
public class ApplicationTests {

    private static File singleLineTestFile;
    private static File fullTestFile;

    @Before
    public void setup() {
        singleLineTestFile = new File("src/test/resources/oneLineTestFile");
        fullTestFile = new File("src/test/resources/fullTestFile");
    }

    @Test(expected = APIException.class)
    public void pack_badPath_shouldThrowExpectedException() throws APIException {
        Packer.pack("/bad/path/file");
    }

    @Test
    public void pack_withGivenTest1File_shouldReturnExpectedResults() throws APIException {
        String filePath = singleLineTestFile.getAbsolutePath();
        String result = Packer.pack(filePath);
        String expectedResult = "\n4";
        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void pack_withGivenTestFile_shouldReturnExpectedResults() throws APIException {
        String filePath = fullTestFile.getAbsolutePath();
        String result = Packer.pack(filePath);
        String expectedResult = "\n4\n-\n2,7\n8,9";
        Assert.assertEquals(expectedResult, result);
    }
}
