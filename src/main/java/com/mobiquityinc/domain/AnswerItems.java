package com.mobiquityinc.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Class to hold possible items with given items.
 *
 * @author sajad
 */
public class AnswerItems {
    /**
     * Possible items.
     */
    private List<Item> items = new ArrayList<>();

    /**
     * Either this traversing is finished or not.
     */
    private boolean finished;

    /**
     * Total cost of items for this path (items).
     */
    private double totalCost;

    /**
     * Total weight of items for this path (items).
     */
    private double totalWeight;

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(double totalCost) {
        this.totalCost = totalCost;
    }

    public double getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(double totalWeight) {
        this.totalWeight = totalWeight;
    }
}
