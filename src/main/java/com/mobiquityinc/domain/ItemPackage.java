package com.mobiquityinc.domain;

import com.mobiquityinc.exception.APIException;

import java.util.List;

import static java.util.Collections.unmodifiableList;

/**
 * Item package class represents attributes of a package.
 *
 * @author sajad
 */
public class ItemPackage {

    /**
     * Maximum weight the package can carry.
     */
    private int maxWeight;

    /**
     * List of items on this package.
     */
    private List<Item> items;

    /**
     * Constructor with validation on assigning values.
     *
     * @param maxWeight The package maximum weight.
     * @param items     List of items on this package.
     * @throws APIException One of the given values are not in the expected range.
     */
    public ItemPackage(int maxWeight, List<Item> items) throws APIException {
        if (maxWeight > 100)
            throw new APIException("Maximum package weight cannot exceed 100");
        if (items.size() > 15)
            throw new APIException("Maximum number of items can't exceed 15");

        this.maxWeight = maxWeight;
        this.items = items;
    }

    public int getMaxWeight() {
        return this.maxWeight;
    }

    public List<Item> getItems() {
        return unmodifiableList(this.items);
    }

    @Override
    public String toString() {
        return "ItemPackage: [maxWeight:" + maxWeight + " , items:" + items + "]";
    }
}
