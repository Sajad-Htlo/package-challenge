package com.mobiquityinc.domain;

import com.mobiquityinc.exception.APIException;

/**
 * Item class encapsulates details of an item.
 *
 * @author sajad
 */
public class Item {

    /**
     * Item index
     */
    private final Integer index;

    /**
     * Item weight
     */
    private final Double weight;

    /**
     * Item cost
     */
    private final Double cost;

    /**
     * Constructor with validation on assigning values.
     *
     * @param index  The item index.
     * @param weight The item weight.
     * @param cost   The item cost.
     * @throws APIException One of the given values are not in the expected range.
     */
    public Item(Integer index, Double weight, Double cost) throws APIException {
        if (weight > 100)
            throw new APIException("Maximum weight of an item cannot exceed 100");
        if (cost > 100)
            throw new APIException("Maximum cost of an item cannot exceed 100");

        this.index = index;
        this.weight = weight;
        this.cost = cost;
    }

    public Integer getIndex() {
        return index;
    }

    public Double getWeight() {
        return weight;
    }

    public Double getCost() {
        return cost;
    }

    @Override
    public String toString() {
        return "Item: [index: " + index + " , weight: " + weight + " , cost: " + cost + " ]";
    }
}
