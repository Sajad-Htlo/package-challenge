package com.mobiquityinc.packer;

import com.mobiquityinc.domain.AnswerItems;
import com.mobiquityinc.domain.Item;
import com.mobiquityinc.domain.ItemPackage;
import com.mobiquityinc.exception.APIException;

import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Packer class.
 *
 * @author sajad
 */
public class Packer {

    /**
     * The smallest weight among items
     */
    private static double smallestWeightItem;

    /**
     * Maximum weight a package can carry.
     */
    private static double packageLimit;

    /**
     * Collection of items in the package
     */
    private static List<Item> items;

    /**
     * Collection of possible answers.
     */
    private static List<AnswerItems> answers;

    /**
     * Answer item for the current items.
     */
    private static AnswerItems answerItems = new AnswerItems();

    /**
     * The starting index (Defaults to 0)
     */
    private static Integer baseIndex = 0;

    /**
     * The highest total cost (most desired items)
     */
    private static double highestTotalCost = 0;

    private Packer() {
    }

    public static String pack(String filePath) throws APIException {
        List<ItemPackage> itemPackages = createItemsPacksForRecords(filePath);

        StringBuilder result = new StringBuilder();

        for (ItemPackage itemPackage : itemPackages) {
            packageLimit = itemPackage.getMaxWeight();

            // Exclude those items their weight is more than the package limit.
            items = itemPackage.getItems().stream().filter(item -> item.getWeight() <= itemPackage.getMaxWeight())
                    .collect(Collectors.toList());

            if (items.isEmpty())
                result.append("\n" + "-");

            else {
                findAllPossibleItems();
                result.append("\n").append(calculateTotalParametersAndFindBestPaths());
            }
        }

        return result.toString();
    }

    /**
     * Read the file at the given {@code url} and create a list of {@linkplain ItemPackage}.
     *
     * @param url Given file address.
     * @return A list of {@linkplain ItemPackage} created with the given file.
     * @throws APIException The file path is not correct.
     */
    private static List<ItemPackage> createItemsPacksForRecords(String url) throws APIException {
        List<ItemPackage> itemPackages = new ArrayList<>();

        File f = new File(url);

        BufferedReader b;
        try {
            b = new BufferedReader(new FileReader(f));
        } catch (FileNotFoundException e) {
            throw new APIException("Incorrect file path");
        }

        String eachLine;
        try {
            while ((eachLine = b.readLine()) != null) {
                int maxWeight = Integer.parseInt(eachLine.split(":")[0].trim());

                List<Item> items = new ArrayList<>();

                // Use a simple regex to find the items on each line
                Matcher m = Pattern.compile("\\((.*?)\\)").matcher(eachLine);
                while (m.find()) {
                    String eachItem = m.group(1);
                    items.add(createItemFor(eachItem));
                }

                // Create a package for each collection of items
                ItemPackage itemPackage = new ItemPackage(maxWeight, items);
                itemPackages.add(itemPackage);
            }
        } catch (IOException ioe) {
            throw new APIException("Incorrect file path");
        }

        return itemPackages;
    }

    /**
     * Creates a new instance of {@linkplain Item} from the given string value.
     * Here, we extract item attributes from the given string.
     *
     * @param eachItem String value delivered from file.
     * @return An instance of {@linkplain Item}.
     * @throws APIException Item criteria not met.
     */
    private static Item createItemFor(String eachItem) throws APIException {
        String[] itemSplittedInfo = eachItem.split(",");

        Integer index = Integer.parseInt(itemSplittedInfo[0]);
        Double weight = Double.parseDouble(itemSplittedInfo[1]);
        Double cost = Double.valueOf(itemSplittedInfo[2].replace("€", ""));

        return new Item(index, weight, cost);
    }

    /**
     * Iterates over {@code items} to find possible next items with the max cost and minimum weight.
     */
    private static void findAllPossibleItems() {
        smallestWeightItem = items.stream().mapToDouble(Item::getWeight).min().getAsDouble();
        answers = new ArrayList<>();

        // find efficient possible items
        for (Item item : items) {
            // find remain weight after this item
            double remainWeight = packageLimit - item.getWeight();

            List<Integer> base = new ArrayList<>();
            base.add(item.getIndex());

            answerItems.getItems().add(item);
            baseIndex = item.getIndex();
            resetAnswerItems();

            // Check if we could go ahead ot not
            if (remainWeight >= smallestWeightItem) {
                findPossibleItems(base, remainWeight);
            } else {
                // Add to the answers
                answerItems.setFinished(true);
                answers.add(answerItems);
                resetAnswerItems();
            }
        }
    }

    /**
     * Resets the {@code answerItems} for each {@linkplain ItemPackage}.
     */
    private static void resetAnswerItems() {
        answerItems = new AnswerItems();
        answerItems.getItems().add(findItemWithIndex(baseIndex, items));
    }

    /**
     * A recursive method to find all possible items to be traversed (select) ti meet the needs.
     * <p>
     * Finally, the {@code answers} collection will populate with the possbile items.
     *
     * @param base  Collection of starting items.
     * @param limit Amount of remain weight to fulfill.
     */
    private static void findPossibleItems(List<Integer> base, double limit) {
        List<Item> remainItems = items.stream().filter(item -> !base.contains(item.getIndex()))
                .collect(Collectors.toList());
        int remainItemsWithNegativeCount = 0;

        for (Item remainItem : remainItems) {
            // Check remain weight
            double remainWeight = limit - remainItem.getWeight();

            if (remainWeight < 0) {
                remainItemsWithNegativeCount++;
                if (remainItemsWithNegativeCount == remainItems.size()) {
                    // Now break the loop and add answers
                    answerItems.setFinished(true);
                    answers.add(answerItems);
                    resetAnswerItems();
                    break;
                } else {
                    // check if we can select another item
                    continue;
                }
            }

            // Add to the answer
            answerItems.getItems().add(remainItem);

            // If can go further
            if (remainWeight >= smallestWeightItem) {
                List<Integer> extendedBase = new ArrayList<>(base);
                extendedBase.add(remainItem.getIndex());
                findPossibleItems(extendedBase, remainWeight);
            } else {
                answerItems.setFinished(true);
                answers.add(answerItems);

                // Initialize answer items and add the base item
                resetAnswerItems();
            }
        }
    }

    /**
     * Returns an instance of {@linkplain Item} based on given index and {@code items}.
     * (Because we sure this item will be existed the given collection, we're not returning
     * an {@linkplain java.util.Optional} of {@linkplain Item}).
     *
     * @param index Given index to look for.
     * @param items Collection of items to search in.
     * @return An instance of {@linkplain Item} based on given index and {@code items}.
     */
    private static Item findItemWithIndex(Integer index, List<Item> items) {
        return items.stream().filter(item -> item.getIndex().equals(index)).findFirst().get();
    }

    /**
     * Calculate total cost/weight for those answer items with more than one item.
     *
     * @return Best items to be selected.
     */
    private static String calculateTotalParametersAndFindBestPaths() {
        highestTotalCost = answers.get(0).getTotalCost();

        answers.forEach(answer -> {
            double totalCost = answer.getItems().stream().mapToDouble(Item::getCost).sum();
            double TotalWeight = answer.getItems().stream().mapToDouble(Item::getWeight).sum();
            answer.setTotalCost(totalCost);
            answer.setTotalWeight(TotalWeight);

            // Update the highest cost if needed
            if (totalCost > highestTotalCost)
                highestTotalCost = totalCost;
        });

        // Find the items with the most cost, choose one with the lower weight if there was more than one item
        List<AnswerItems> bestItems = answers.stream()
                .sorted(Comparator.comparing(AnswerItems::getTotalWeight))
                .filter(ai -> ai.getTotalCost() == highestTotalCost)
                .collect(Collectors.toList());

        String bastPath;
        if (bestItems.size() > 1) {
            // Then resort equal items
            bastPath = bestItems.get(0).getItems().stream().map(Item::getIndex)
                    .sorted(Integer::compareTo).map(Object::toString)
                    .collect(Collectors.joining(","));
        } else {
            bastPath = bestItems.get(0).getItems().stream().map(Item::getIndex).map(Object::toString)
                    .collect(Collectors.joining(","));
        }

        return bastPath;
    }

}
