package com.mobiquityinc;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.packer.Packer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

    public static void main(String[] args) throws APIException {
        SpringApplication.run(Application.class, args);

        String finalResult = Packer.pack("src/main/resources/file.txt");

        System.out.println("Final Result: " + finalResult);
    }
}
